package providertags

//go:generate sh -c "curl --silent https://raw.githubusercontent.com/protocolbuffers/protobuf/3d995aaf98ed73ddcbbf0ed88d82bbbf80ec8cf9/src/google/protobuf/wrappers.proto > wrappers.proto"
//go:generate sh -c "curl --silent https://raw.githubusercontent.com/protocolbuffers/protobuf/3d995aaf98ed73ddcbbf0ed88d82bbbf80ec8cf9/src/google/protobuf/timestamp.proto > timestamp.proto"
//go:generate sh -c "curl --silent https://raw.githubusercontent.com/protocolbuffers/protobuf/3d995aaf98ed73ddcbbf0ed88d82bbbf80ec8cf9/src/google/protobuf/descriptor.proto > descriptor.proto"
//go:generate protoc --go_out=. --go_opt=paths=source_relative --descriptor_set_out=providertags.pb providertags.proto wrappers.proto timestamp.proto
//go:generate sh -c "protoc --decode=google.protobuf.FileDescriptorSet descriptor.proto < providertags.pb > providertags.pbtxt"
//go:generate rm ./providertags.pb ./timestamp.pb.go ./wrappers.pb.go
